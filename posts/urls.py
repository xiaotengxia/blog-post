from django.urls import path
from posts.views import (
    list_posts,
    creat_post,
    delete_post,
    detail_post,
    upgrade_post,
)

urlpatterns = [
    path("", list_posts, name="posts_list"),
    path("new/", creat_post, name="post_create"),
    path("<int:id>/delete", delete_post, name="post_delete"),
    path("<int:id>/detail", detail_post, name="post_detail"),
    path("<int:id>/upgrade", upgrade_post, name="post_upgrade"),

]