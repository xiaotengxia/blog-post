from django.shortcuts import render, redirect
from posts.models import Post
from posts.forms import PostForm

# Create your views here.
def list_posts(request):
    posts = Post.objects.all()
    context = {
        "posts": posts,
    }

    return render(request, "posts/main.html", context)


def detail_post(request, id):
    post = Post.objects.get(id=id)
    context = {
        "post": post,
    }
    return render(request, "posts/detail.html", context)


def creat_post(request):
    form = PostForm()
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect("posts_list")
    context = {
        "form": form,
    }
    return render(request, "posts/create.html", context)


def delete_post(request, id):
    post = Post.objects.get(pk=id)
    if request.method == "POST":
        post.delete()
        return redirect("posts_list")
    context = {
        "post": post,
    }
    return render(request, "posts/delete.html", context)


def upgrade_post(request, id):
    post = Post.objects.get(pk=id)
    form = PostForm(request.POST or None, instance=post)

    if form.is_valid():
        form.save()
        return redirect("posts_list")

    context = {
        "form": form,
    }
    return render(request, "posts/upgrade.html", context)