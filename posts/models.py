from tkinter import CASCADE
from django.db import models
from django.conf import settings


USER_MODEL = settings.AUTH_USER_MODEL

# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=100)
    avatar = models.URLField(null=True, blank=True)
    author = models.ForeignKey(USER_MODEL, related_name="posts", on_delete=models.CASCADE, null=True)
    content = models.TextField()
    image = models.URLField(null=True, blank=True)
    creat = models.DateTimeField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)
